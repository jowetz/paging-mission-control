import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneOffset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Class defines a SatelliteReading object
 *
 * Takes input from temperature and battery level readings. 
 * Provides methods to check and report if reading values are outside of defined limits.
 * 
 * @author Joel Wetzsteon
 */
public class SatelliteReading {

	private transient LocalDateTime fmtTimestamp;	
	private int satelliteId;
	private transient float redHighLimit;
	private transient float yellowHighLimit;
	private transient float yellowLowLimit;
	private transient float redLowLimit;
	private transient float rawValue;
	private String component;
	private String timestamp = "";
	private String severity = "";

	/**
	 * Constructor takes an input string of format
	 * "<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>"
	 */
	public SatelliteReading(String reading) {
		String[] splitReading = reading.split("\\|", 8);
		this.fmtTimestamp = LocalDateTime.parse(splitReading[0], DateTimeFormatter.ofPattern(
                                          "yyyyMMdd HH:mm:ss.SSS"));
		this.satelliteId = Integer.parseInt(splitReading[1]);
		this.redHighLimit = Float.parseFloat(splitReading[2]);
		this.yellowHighLimit = Float.parseFloat(splitReading[3]);
		this.yellowLowLimit = Float.parseFloat(splitReading[4]);
		this.redLowLimit = Float.parseFloat(splitReading[5]);
		this.rawValue = Float.parseFloat(splitReading[6]);
		this.component = splitReading[7];

		this.timestamp = fmtTimestamp.toString() + "Z";
	}

	/**
	 * Getter Methods
	 */
	public LocalDateTime getFmtTimestamp() { return this.fmtTimestamp; }
	public int getSatelliteId() { return this.satelliteId; }
	public float getRedHighLimit() { return this.redHighLimit; }
	public float getYellowHighLimit() { return this.yellowHighLimit; }
	public float getYellowLowLimit() { return this.yellowLowLimit; }
	public float getRedLowLimit() { return this.redLowLimit; }
	public float getRawValue() { return this.rawValue; }
	public String getComponent() { return this.component; }
	public String getTimestamp() { return this.timestamp; }
	public String getSeverity() { return this.severity; }

	/**
	 * Returns milliseconds since Epoch for an alert's timestamp
	 */
	public long getMilliseconds() {
		return this.fmtTimestamp.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
	}	

	/**
	 * Setter Methods not implemented, reading data should not be manipulated
	 */

	/**
	 * Runs against temperature readings and returns true if raw reading is above Red High
	 * limit.
	 * 
	 * @exception Exception if method not ran against a temperature reading
	 */
	public boolean checkTempRedHigh() throws Exception {
		//Verify this is checking a temperature reading
		if (!component.equalsIgnoreCase("TSTAT")) 
			throw new Exception("Method only valid for Temperature Readings");

		if (rawValue - redHighLimit > 0) {
			severity = "RED HIGH";
			return true;
		} else 
			return false;
	}

	/**
	 * Runs against battery readings and returns true if raw reading is below Red Low
	 * limit.
	 * 
	 * @exception Exception if method not ran agains a battery reading
	 */
	public boolean checkBattRedLow() throws Exception {
		//Verify this is checking a battery reading
		if (!component.equalsIgnoreCase("BATT"))
			throw new Exception("Method only valid for Battery Readings");
		
		if (redLowLimit - rawValue > 0) {
			severity = "RED LOW";
			return true;
		} else
			return false;
	}	

	/**
	 * Serializes pretty print json object with required alert fields.
	 */
	public String outputJsonAlert() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		Gson gson = builder.create();
		return gson.toJson(this).toString();
	}
}
