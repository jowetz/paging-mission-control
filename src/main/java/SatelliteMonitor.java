import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Driver class for SatelliteMonitor application
 *
 * Takes satellite reading text data as filepath passed by command line argument and
 * handles temperature and battery readings, alerting as required.
 *
 * @author Joel Wetzsteon
 */
public class SatelliteMonitor {
	//Queues will only store three RED readings, assuming input readings are sequential.
	//Earliest readings are removed first, and readings aren't stored long term after 
	//they have been checked for alert condition.
	
	//Satellite 1000 reading queues
	private Queue<SatelliteReading> satellite1000Temp = new LinkedList<>();
	private Queue<SatelliteReading> satellite1000Batt = new LinkedList<>();

	//Satellite 1001 reading queues
	private Queue<SatelliteReading> satellite1001Temp = new LinkedList<>();
	private Queue<SatelliteReading> satellite1001Batt = new LinkedList<>();

	/**
	 * Constructor takes a String filename with reading data to parse.
	 *
	 * @param filename String path to file with reading text data
	 */
	public SatelliteMonitor(String filename) {
		try {
			//Readings are evaluated for alert condition line by line
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String reading;
			while ((reading = br.readLine()) != null) {
				SatelliteReading read = new SatelliteReading(reading);
				
				//Check if temperature reading or battery reading
				if (read.getComponent().equalsIgnoreCase("TSTAT")) {
					handleTemperatureReading(read);
				} else if (read.getComponent().equalsIgnoreCase("BATT")) {
					handleBatteryReading(read);
				} else {
					System.out.println("Unrecognized reading type");
				}		
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Defines what happens to temperature readings, component='TSTAT'
	 *
	 * @param read SatelliteReading object to evaluate
	 */ 
	private void handleTemperatureReading(SatelliteReading read) {
		//Determine satellite queue to send SatelliteReading to
		Queue<SatelliteReading> queue = null;
		if (read.getSatelliteId() == 1000)
			queue = satellite1000Temp;
		else if (read.getSatelliteId() == 1001)
			queue = satellite1001Temp;

		//If temperature over Red High threshold check if alert needs to be sent
		try {
			if (read.checkTempRedHigh()) {
				queue.add(read);
				handleAlert(queue, read);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
         * Defines what happens to battery readings, component='BATT'
         *
         * @param read SatelliteReading object to evaluate
         */
	private void handleBatteryReading(SatelliteReading read) {
		//Determine satellite queue to send SatelliteReading to
		Queue<SatelliteReading> queue = null;
                if (read.getSatelliteId() == 1000)
                        queue = satellite1000Batt;
                else if (read.getSatelliteId() == 1001)
                        queue = satellite1001Batt;

		//If battery level below Red Low threshold check if alert needs to be sent	
		try {
			if (read.checkBattRedLow()) {
				queue.add(read);
				handleAlert(queue, read);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Defines how alerts are processed.
	 * Checks red alert SatelliteReadings in the specified queue and if they fall within
	 * a five minute window an alert will be output to the console.
	 *
	 * @param queue SatelliteReading queue to check reading timestamps within
	 * @param lastAdded SatelliteReading latest reading added to queue. 
	 * 	Provides access to reading at end of queue.
	 */ 
	private void handleAlert(Queue<SatelliteReading> queue, SatelliteReading lastAdded) {
		//Less than three readings will not cause any alerts to fire
		if (queue.size() < 3)
			return;

		//Drop earliest alert if more than three stored
		if (queue.size() > 3)
			queue.remove();
		
		//Check if all three alerts are in 5 minute window
		//If they are print the json alert output of the earliest alert (head of queue)
		if (lastAdded.getMilliseconds() - queue.peek().getMilliseconds() <= 300000)
			System.out.println(queue.peek().outputJsonAlert());
	}

	/**
	 * Program entry. Takes filename specified by first command line argument
	 * to instantiate SatelliteMonitor.
	 */
	public static void main(String[] args) {
		//Validate that filename was passed as command line argument
		if (args.length != 1) {
			System.out.println("Usage: SatelliteMonitor input_file");
			System.exit(0);
		}
		SatelliteMonitor satMonitor = new SatelliteMonitor(args[0]);
	}
}
