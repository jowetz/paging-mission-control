import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests to validate SatelliteReading class methods
 *
 * @author Joel Wetzsteon
 */
public class SatelliteReadingTests {

	String testString = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";
	SatelliteReading testReading = new SatelliteReading(testString);
	
	/**
	 * Test Getter methods for proper return values
	 */
	@Test
	public void testGetters() {
		assertEquals("2018-01-01T23:01:05.001Z", testReading.getTimestamp(), "Should return expected date format");
		assertEquals(1001, testReading.getSatelliteId(), "ID: 1001");
		assertEquals(101, testReading.getRedHighLimit(), "RH Limit: 101");
		assertEquals(98, testReading.getYellowHighLimit(), "YH Limit: 98");
		assertEquals(25, testReading.getYellowLowLimit(), "YL Limit: 25");
		assertEquals(20, testReading.getRedLowLimit(), "RL Limit: 20");
		assertEquals(99.9, testReading.getRawValue(), 0.01);
		assertEquals("TSTAT", testReading.getComponent(), "Comp: TSTAT");

	}

	String tempStringRed = "20180101 23:01:05.001|1001|101|98|25|20|101.1|TSTAT";
	String tempStringNoRed = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";

	SatelliteReading tempAlert = new SatelliteReading(tempStringRed);
	SatelliteReading tempNoAlert = new SatelliteReading(tempStringNoRed);

	/**
	 * Test functionality of temperature Red High checks true and false
	 */
	@Test
	public void testTempReadings() {
		try {
			assertEquals(true, tempAlert.checkTempRedHigh(), "Evaluates to true");
			assertEquals(false, tempNoAlert.checkTempRedHigh(), "Evaluates to False");
		} catch (Exception e) {

		}
	}

	String battStringRed = "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT";
        String battStringNoRed = "20180101 23:01:09.521|1000|17|15|9|8|8.1|BATT";

        SatelliteReading battAlert = new SatelliteReading(battStringRed);
        SatelliteReading battNoAlert = new SatelliteReading(battStringNoRed);

	/**
	 * Test functionality of battery Red Low checks true and false
	 */
	@Test
        public void testBattReadings() {
                try {
                        assertEquals(true, battAlert.checkBattRedLow(), "Evaluates to true");
                        assertEquals(false, battNoAlert.checkBattRedLow(), "Evaluates to False");
                } catch (Exception e) {

                }
        }
}
